package com.gumeteapps.citation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gumeteapps.citation.authantication.AuthRequestSendManager;
import com.gumeteapps.citation.citationfetching.CitationFetchManager;
import com.gumeteapps.citation.periodicfetchcitation.PeriodicFetchCitation;

public class Home extends AppCompatActivity {

    public static final String TAG = "mydebug";

    private NavController navController;

    private ActivityViewModel viewModel;

    public static final String EXTRA_CITATION = "citation";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navController = Navigation.findNavController(this, R.id.nav_host);
        setupNavBar();
        setupToolBar();

        Navigation.findNavController(this, R.id.nav_host).navigate(R.id.action_global_activityDayCitation);

        // Initialisation du ViewModel pour conserver nos donnees d'activite
        viewModel = ViewModelProviders.of(this).get(ActivityViewModel.class);
    }

    private void setupNavBar() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    private void setupToolBar() {
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        Toolbar toolbar = findViewById(R.id.toolbar);
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
    }

    // TODO : implementer onDestroy() pour tout nettoyer

    @Override
    protected void onStart() {
        super.onStart();

        // Au debut,
        Intent intent = getIntent();
        if(intent != null && intent.getExtras() != null && intent.hasExtra(EXTRA_CITATION)) {
            // Si on recoit une Citation a travers l'Intent, on l'affiche et on ne demande plus rien
            String citation = intent.getExtras().getString(EXTRA_CITATION);
            viewModel.getCitationData().setValue(citation);
        }
        else {
            /*Si on ne recoit rien d'interressant via l'Intent, on verifi si une instance precedente de l'application
             * n'a pas laisse une citation dans le viewModel*/
            String citation = viewModel.getCitationData().getValue();
            if (citation == null || citation.equals(getString(R.string.message_cannot_fetch_citation))) {
                /*Si le viewModel ne contient rien (On est peut etre la premiere instance) ou
                 * Si le viewModel contient une erreur, on demande au CitationFetchManager
                 * de nous apporter une Citation (Si il peut) on attend sa reponse via onFetchListener*/
                int res = CitationFetchManager.instance.fetch(this, onFetchListener);
                if(res == -1) {
                    // Si on n'est pas connecter, on essaie de se connecter puis on refait un essaie.
                    tryAfterConnect();
                }
            }
        }

        /*Ici on s'occupe du Worker qui cherche des citations en permanence
         * Il peut avoir ete cancel ou autre chose on s'assure donc qu'il est bien sur pieds*/
        PeriodicFetchCitation.doCheckWorker(this);
    }

    /*Fonction pour se connecter puis faire une demande de citation*/
    private void tryAfterConnect() {
        AuthRequestSendManager.sendForUi(this, new AuthRequestSendManager.OnConnectionSuccess() {
            @Override
            public void onConnect() {
                // On s'est connecte
                int res = CitationFetchManager.instance.fetch(Home.this, onFetchListener);
                if (res == -1) {
                    // On a un probleme
                    viewModel.getCitationData().setValue(getString(R.string.message_cannot_connect));
                }
            }
        }, new AuthRequestSendManager.OnConnectionFail() {
            @Override
            public void onFail() {
                viewModel.getCitationData().setValue(getString(R.string.message_cannot_connect));
            }
        });
    }

    private CitationFetchManager.OnEndListener onFetchListener = new CitationFetchManager.OnEndListener() {
        @Override
        public void onResult(String citation) {
            // Lorsqu'on obtient la citation du serveur, on met a jour la citation de l'activite
            viewModel.getCitationData().setValue(citation);
        }

        @Override
        public void onFail() {
            // Si le serveur n'a pas pu repondre, on affiche un message
            viewModel.getCitationData().setValue(getString(R.string.message_cannot_fetch_citation));
        }
    };
}
