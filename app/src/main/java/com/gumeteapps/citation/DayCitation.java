package com.gumeteapps.citation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/** Cette Activite :
 *  - demander A un CitationFetchManager de recuperer une Citation au server et de les lui retourne
 *  - Il affiche la citation retourner
 *  - Il affiche un message d'erreur si aucune Citation n'est retournee
 * */
public class DayCitation extends Fragment {

    public static final String TAG = "mydebug";

    private TextView textViewCitation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day_citation, container, false);
        textViewCitation = view.findViewById(R.id.txtv_day_citation);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialisation du ViewModel pour conserver nos donnees d'activite
        ActivityViewModel viewModel = ViewModelProviders.of(requireActivity()).get(ActivityViewModel.class);
        viewModel.getCitationData().observe(requireActivity(), dataObserver);
    }

    /*L'observeur des donnees du viewModel de l'activite*/
    private Observer<String> dataObserver = new Observer<String>() {
        @Override
        public void onChanged(String s) {
            /*A chaque modification de notre Citation dans le ViewModel, on l'affiche*/
            textViewCitation.setText(s);
        }
    };
}
