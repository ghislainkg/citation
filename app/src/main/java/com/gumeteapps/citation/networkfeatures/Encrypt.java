package com.gumeteapps.citation.networkfeatures;

import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encrypt {

    public static final String TAG = "mydebug";

    private static String privatekey = "M";
    private static String privateiv = "My iv";

    public static String encrypt(String msg) {
        Log.i(TAG, "encrypt: "+msg);

        if(msg.length() % 16 != 0) {
            int padsize = ((int)msg.length() / 16 + 1)*16 - msg.length();
            for (int i = 0; i < padsize; i++) {
                msg += " ";
            }
        }

        byte[] text = msg.getBytes();
        byte[] res = "".getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(privatekey.getBytes());
            byte[] privatekeyhash = md.digest();
            md.reset();
            md.update(privateiv.getBytes());
            byte[] ivhash = md.digest();

            SecretKey key = new SecretKeySpec(privatekeyhash, 0, 32, "AES");
            IvParameterSpec iv = new IvParameterSpec(ivhash, 0, 16);

            Cipher cipher = Cipher.getInstance("AES_256/CBC/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            res = cipher.doFinal(text);
        } catch (Exception e) {
            Log.e(TAG, "encrypt: ", e);
        }

        return Base64.encodeToString(res, 0);
    }

    /*msg is encoded in base64*/
    public static String decrypt(String msg) {
        if(msg.length() % 16 != 0) {
            int padsize = ((int)msg.length() / 16 + 1)*16 - msg.length();
            for (int i = 0; i < padsize; i++) {
                msg += " ";
            }
        }

        byte[] text =  Base64.decode(msg, 0);
        byte[] res = "".getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(privatekey.getBytes());
            byte[] privatekeyhash = md.digest();
            md.reset();
            md.update(privateiv.getBytes());
            byte[] ivhash = md.digest();

            SecretKey key = new SecretKeySpec(privatekeyhash, 0, 32, "AES");
            IvParameterSpec iv = new IvParameterSpec(ivhash, 0, 16);

            Cipher cipher = Cipher.getInstance("AES_256/CBC/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            res = cipher.doFinal(text);
        }
        catch (Exception e) {
            Log.e(TAG, "decrypt: ", e);
        }

        return new String(res);
    }
}
