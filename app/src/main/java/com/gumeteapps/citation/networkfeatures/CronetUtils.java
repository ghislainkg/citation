package com.gumeteapps.citation.networkfeatures;

import android.content.Context;
import android.util.Log;

import org.chromium.net.CronetEngine;
import org.chromium.net.UploadDataProvider;
import org.chromium.net.UploadDataSink;
import org.chromium.net.UrlRequest;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CronetUtils {

    private static final String TAG = "mydebug";

    public static void makeRequest(Context context, String url, CronetRequestCallBack callBack) {
        CronetEngine.Builder cronetBuilder = new CronetEngine.Builder(context);
        cronetBuilder.enableHttp2(true);
        CronetEngine cronetEngine = cronetBuilder.build();

        Executor executor = Executors.newSingleThreadExecutor();

        UrlRequest.Builder requestBuilder = cronetEngine.newUrlRequestBuilder(
                url,
                callBack,
                executor);
        UrlRequest request = requestBuilder.build();

        cronetEngine.createURLStreamHandlerFactory();

        request.start();
    }

    public static void makeRequest(Context context, String url, String toSend, CronetRequestCallBack callBack) {
        Log.i(TAG, "makeRequest: "+toSend);
        toSend = Encrypt.encrypt(toSend);
        Log.i(TAG, "makeRequest: Crypted = "+toSend);

        CronetEngine.Builder cronetBuilder = new CronetEngine.Builder(context);
        cronetBuilder.enableHttp2(true);
        CronetEngine cronetEngine = cronetBuilder.build();

        Executor executor = Executors.newSingleThreadExecutor();

        UrlRequest.Builder requestBuilder = cronetEngine.newUrlRequestBuilder(
                url,
                callBack,
                executor);
        UrlRequest request = requestBuilder
                .setUploadDataProvider(new Uploader(toSend), executor)
                .addHeader("Content-Type", "text/plain")
                .build();

        cronetEngine.createURLStreamHandlerFactory();

        request.start();
    }

    private static class Uploader extends UploadDataProvider {

        byte[] toSend;

        Uploader(String toSend) {
            this.toSend = toSend.getBytes();
        }

        @Override
        public long getLength() throws IOException {
            return toSend.length;
        }

        @Override
        public void read(UploadDataSink uploadDataSink, ByteBuffer byteBuffer) throws IOException {
            byteBuffer.put(toSend);
            uploadDataSink.onReadSucceeded(false);
        }

        @Override
        public void rewind(UploadDataSink uploadDataSink) throws IOException {
            uploadDataSink.onRewindSucceeded();
        }
    };

}
