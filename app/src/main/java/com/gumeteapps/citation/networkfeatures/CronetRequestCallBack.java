package com.gumeteapps.citation.networkfeatures;

import android.util.Log;

import org.chromium.net.CronetException;
import org.chromium.net.UrlRequest;
import org.chromium.net.UrlResponseInfo;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public abstract class CronetRequestCallBack extends UrlRequest.Callback {

    public static final String TAG = "mydebug";

    private static final int BUFFER_SIZE = 128;

    private String response = "";

    public abstract void onSuccess(String response);
    public abstract void onFailure(int code);

    @Override
    public void onRedirectReceived(UrlRequest request, UrlResponseInfo info, String newLocationUrl) {
        request.followRedirect();
    }

    @Override
    public void onResponseStarted(UrlRequest request, UrlResponseInfo info) {
        request.read(ByteBuffer.allocateDirect(BUFFER_SIZE));
    }

    @Override
    public void onReadCompleted(UrlRequest request, UrlResponseInfo info, ByteBuffer byteBuffer) {
        collectReads(byteBuffer);
        request.read(ByteBuffer.allocateDirect(BUFFER_SIZE));
    }

    @Override
    public void onSucceeded(UrlRequest request, UrlResponseInfo info) {
        if(info.getHttpStatusCode() >= 400) {
            // Le serveur retourne une erreur
            Log.i(TAG, "onSucceeded: Error code :"+info.getHttpStatusCode());
            onFailure(info.getHttpStatusCode());
        }
        else {
            // response = response.trim();
            Log.i(TAG, "onSucceeded: Fresh Response :"+response);
            response = Encrypt.decrypt(response);
            Log.i(TAG, "onSucceeded: Decrypted Response :"+response);
            onSuccess(response);
        }
    }

    @Override
    public void onFailed(UrlRequest request, UrlResponseInfo info, CronetException error) {
        onFailure(info.getHttpStatusCode());
    }

    private void collectReads(ByteBuffer byteBuffer) {
        String chunk = new String(byteBuffer.array(), StandardCharsets.UTF_8);
        response += chunk;
    }
}
