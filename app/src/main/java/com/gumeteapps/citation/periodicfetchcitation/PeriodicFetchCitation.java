package com.gumeteapps.citation.periodicfetchcitation;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PeriodicFetchCitation {

    public static final String TAG = "mydebug";

    private static PeriodicWorkRequest request;
    private static String PERIODIC_TAG = "com.gumeteapps.citation.notificationchannel.periodicrequestid";

    private static void scheduleWork(Context context) {
        Log.i(TAG, "scheduleWork: ");
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        request = new PeriodicWorkRequest.Builder(
                PeriodicFetchCitationWorker.class,
                1,
                TimeUnit.MINUTES)
                .addTag(PERIODIC_TAG)
                .setConstraints(constraints)
                .build();

        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueue(request);
    }

    private static void rescueWork(Context context) {
        Log.i(TAG, "rescueWork: ");
        Constraints constraints = new Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest rescueRequest = new OneTimeWorkRequest.Builder(PeriodicFetchCitationWorker.class)
                .setConstraints(constraints)
                .build();
        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueue(rescueRequest);
    }

    private static class WorkObserver implements Observer<WorkInfo> {

        Context context;
        WorkObserver(Context context) {
            this.context = context;
        }

        @Override
        public void onChanged(WorkInfo workInfo) {
            Log.i(TAG, "onChanged: "+workInfo.getState());
            if(workInfo.getState() == WorkInfo.State.FAILED) {
                Log.i(TAG, "onChanged: Failed");
                rescueWork(context);
            }
            else if(workInfo.getState() == WorkInfo.State.CANCELLED) {
                Log.i(TAG, "onChanged: Cancleled");
                scheduleWork(context);
            }
        }
    }

    public static <T extends Context & LifecycleOwner> void doCheckWorker(T owner) {
        Log.i(TAG, "doCheckWorker: ");

        WorkManager workManager = WorkManager.getInstance(owner);

        ListenableFuture<List<WorkInfo>> workerLiveData = workManager.getWorkInfosByTag(PERIODIC_TAG);

        List<WorkInfo> workInfos = null;
        try {
            workInfos = workerLiveData.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(workInfos == null || workInfos.size() == 0) {
            Log.i(TAG, "doCheckWorker: 1");
            scheduleWork(owner);
            WorkObserver observer = new WorkObserver(owner);
            workManager.getWorkInfoByIdLiveData(request.getId()).observe(owner, observer);
        }
    }
}
