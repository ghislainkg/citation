package com.gumeteapps.citation.periodicfetchcitation;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.gumeteapps.citation.DayCitation;
import com.gumeteapps.citation.R;

/*Ce service recoit et affiche une citation dans une notification*/
public class PeriodicFetchCitationNotificationService extends IntentService {

    public static final String TAG = "mydebug";

    public static final String EXTRA_CITATION = "citation";
    public static final String SERVICE_NAME = "com.gumeteapps.citation.periodic_fetch_citation_notification_service";

    public PeriodicFetchCitationNotificationService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null && intent.getExtras() != null && intent.hasExtra(EXTRA_CITATION)) {
            // On recoit la citation a afficher dans la notification
            final String citation = intent.getExtras().getString(EXTRA_CITATION);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    // On affiche la notification avec la citation
                    Log.i(TAG, "run: ");
                    throwNotification(citation);
                }
            });
            t.start();
        }
    }

    private void throwNotification(String citation) {
        // On affiche la notification

        // Si on clique sur la notification, on vas afficher la citation de cette notification dans
        // l'activite ActivityDayCitation
        Intent intent = new Intent(this, DayCitation.class);
        intent.putExtra(EXTRA_CITATION, citation);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        String CHANNEL_ID = getString(R.string.notification_channel_id);
        String CHANNEL_NAME = getString(R.string.notification_channel_name);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if(notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        int NOTIF_ID = 564;
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.notif_title_new_citation))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText(citation)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIF_ID, notification);
    }
}
