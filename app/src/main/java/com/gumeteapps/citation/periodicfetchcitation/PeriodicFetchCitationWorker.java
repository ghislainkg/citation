package com.gumeteapps.citation.periodicfetchcitation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.gumeteapps.citation.authantication.AuthRequestSendManager;
import com.gumeteapps.citation.citationfetching.CitationFetchManager;
import com.gumeteapps.citation.networkfeatures.CronetRequestCallBack;
import com.gumeteapps.citation.networkfeatures.CronetUtils;

public class PeriodicFetchCitationWorker extends Worker {

    public static final String TAG = "mydebug";

    private Context context;

    public PeriodicFetchCitationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);

        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                String requete = CitationFetchManager.getCitationRequest(context);
                if(requete == null) {
                    AuthRequestSendManager.send(context, new AuthRequestSendManager.OnConnectionSuccess() {
                        @Override
                        public void onConnect() {
                            String newrequete = CitationFetchManager.getCitationRequest(context);
                            CronetUtils.makeRequest(context, newrequete,
                                    CitationFetchManager.SERVER_URL_REQUEST_CITATION, callBack);
                        }
                    });
                }
                else {
                    CronetUtils.makeRequest(context, requete,
                            CitationFetchManager.SERVER_URL_REQUEST_CITATION, callBack);
                }
            }
        });
        t.start();

        return Result.success();
    }

    private CronetRequestCallBack callBack = new CronetRequestCallBack() {
        @Override
        public void onSuccess(String response) {

            // TODO : Ecrire les donnees recus dans un fichier/base de donnees/ key-value file/ etc ...

            Intent intent = new Intent(context, PeriodicFetchCitationNotificationService.class);
            intent.putExtra(PeriodicFetchCitationNotificationService.EXTRA_CITATION, response);
            context.startService(intent);

            Log.i(TAG, "onSuccess: Periodic");
        }

        @Override
        public void onFailure(int code) {

        }
    };
}
