package com.gumeteapps.citation;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gumeteapps.citation.authantication.AuthRequestSendManager;
import com.gumeteapps.citation.citationfetching.CitationFetchManager;

public class MakeCitation extends Fragment {

    public static final String TAG = "mydebug";

    private EditText edtCitation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_make_citation, container, false);

        edtCitation = view.findViewById(R.id.edt_make_citation);
        Button btnCitation = view.findViewById(R.id.btn_make_citation);
        TextView txtvInfo = view.findViewById(R.id.txtv_make_citation_infos);

        btnCitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int res = CitationFetchManager.instance.send(
                        getActivity(),
                        edtCitation.getText().toString(),
                        onRequestResult);
                if(res == -1) {
                    // On n'est pas connecte, on essaie de se connecter
                    AuthRequestSendManager.sendForUi(requireContext(), new AuthRequestSendManager.OnConnectionSuccess() {
                        @Override
                        public void onConnect() {
                            // On est connecte, on refait un essaie d'envoie
                            int newres = CitationFetchManager.instance.send(
                                    getActivity(),
                                    edtCitation.getText().toString(),
                                    onRequestResult);
                            if(newres == -1) {
                                // On a un probleme
                                Toast.makeText(getActivity(), R.string.message_cannot_connect, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new AuthRequestSendManager.OnConnectionFail() {
                        @Override
                        public void onFail() {
                            Toast.makeText(getActivity(), R.string.message_cannot_connect, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        return view;
    }

    private CitationFetchManager.OnEndListener onRequestResult = new CitationFetchManager.OnEndListener() {
        @Override
        public void onResult(String res) {
            Toast.makeText(getActivity(), "Votre citation est envoye", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFail() {
            Toast.makeText(getActivity(), "Votre citation n'est pas envoye", Toast.LENGTH_LONG).show();
        }
    };
}
