package com.gumeteapps.citation.authantication;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.gumeteapps.citation.networkfeatures.CronetRequestCallBack;
import com.gumeteapps.citation.networkfeatures.CronetUtils;

import java.util.Date;

/*Cette classe s'petmet d'envoyer des requetes de connection
et de creation de nouvel utilisateur au serveur en cas de premiere communication.
Quand tout se passe bien, elle recoit un token du serveur qu'elle stocke dans un fichier de preference*/
public class AuthRequestSendManager {

    public static final String TAG = "mydebug";

    // L'url de connection au serveur; (Elle sert aussi a la creation d'un nouvel utilisateur)
    private static final String SERVER_URL = "https://warm-shore-29934.herokuapp.com/connect";
    //private static final String SERVER_URL = "http://192.168.43.251:5000/connect";

    // Le fichier de preference pour le login et le mot de passe
    private static final String CREDENTIALS_LOCATION = "com.gumeteapps.citation.credentials";
    // La cle du login et mot de passe dans le fichier de preference
    private static final String LOGIN_KEY = "login";
    private static final String PASSWORD_KEY = "password";

    private static boolean isNewUser;

    /*Retourne une requete de creation de nouvel utilisateur prete a envoyer (apres l'avoir chiffre)*/
    private static String getNewUserRequest() {
        String type = "n";
        String token = "-1";

        // Les identifiants du nouvel utilisateur
        // TODO : Rendre ces identifiants aleatoires
        Date d = new Date();
        String login = d.getTime()+"";
        String password = d.getTime()+"";

        return type+"|"+token+"|"+login+"@"+password;
    }

    /*Retourne une requete de creation de connection prete a envoyer*/
    private static String getConnectRequest(String login, String password) {
        String type = "c";
        String token = "-1";

        return type+"|"+token+"|"+login+"@"+password;
    }

    public interface  OnConnectionSuccess {
        void onConnect();
    }
    public interface  OnConnectionFail {
        void onFail();
    }

    /*Fonction pour envoyer une demande de connection au serveur.
    * A ne pas utiliser dans le ThreadUI.*/
    public static void send(final Context context, final OnConnectionSuccess onConnectionSuccess, final OnConnectionFail onConnectionFail) {
        // On lit le fichier de preference des identifiants.
        SharedPreferences sharedPref = context.getSharedPreferences(
                CREDENTIALS_LOCATION, Context.MODE_PRIVATE);
        final String login = sharedPref.getString(LOGIN_KEY, null);
        final String password = sharedPref.getString(PASSWORD_KEY, null);

        String requestStr;
        if(login == null || password == null) {
            // Si on n'a pas encore d'identifiant, on vas demander la creation d'un nouvel utilisateur.
            requestStr = getNewUserRequest();
            isNewUser = true;
        }
        else {
            // Si on a des identifants, on vas faire une demande de connection
            requestStr = getConnectRequest(login, password);
        }

        // On envoie la requete
        CronetUtils.makeRequest(context, SERVER_URL, requestStr, new CronetRequestCallBack() {
            @Override
            public void onSuccess(String response) {
                // On recoit la reponse du serveur.

                // On sauvegarde le nouveau token.
                TokenManager.saveToken(context, response);

                if(isNewUser) {
                    // Si on a fait une demande de nouvel utilisateur, on enregistre les identifants
                    SharedPreferences sharedPref = context.getSharedPreferences(
                            CREDENTIALS_LOCATION, Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(LOGIN_KEY, login);
                    editor.putString(PASSWORD_KEY, password);
                    editor.apply();
                }

                if(onConnectionSuccess != null) {
                    onConnectionSuccess.onConnect();
                }
            }

            @Override
            public void onFailure(int code) {
                if(onConnectionFail != null) {
                    onConnectionFail.onFail();
                }
            }
        });
    }

    /*Fonction pour envoyer une demande de connection au serveur.
     * A ne pas utiliser dans le ThreadUI.*/
    public static void send(final Context context, final OnConnectionSuccess onConnectionSuccess) {
        send(context, onConnectionSuccess, null);
    }

    /*Fonction pour envoyer une demande de connection au serveur.
    * Les listener sont appeles dans le threadUI*/
    public static void sendForUi(final Context context, final OnConnectionSuccess onConnectionSuccess, final OnConnectionFail onConnectionFail) {
        final int FAIL = 0;
        final int SUCCESS = 1;

        /*Le handler qui execute les listener dans le threadUI*/
        final Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what) {
                    case FAIL:
                        if(onConnectionFail != null) {
                            onConnectionFail.onFail();
                        }
                        break;
                    case SUCCESS:
                        if(onConnectionSuccess != null) {
                            onConnectionSuccess.onConnect();
                        }
                        break;
                    default:
                        break;
                }
                super.handleMessage(msg);
            }
        };
        send(context,
            new OnConnectionSuccess() {
                @Override
                public void onConnect() {
                    // On vas dans le ThreadUI
                    Message message = handler.obtainMessage(SUCCESS);
                    message.sendToTarget();
                }
            },
            new OnConnectionFail() {
                @Override
                public void onFail() {
                    // On vas dans le ThreadUI
                    Message message = handler.obtainMessage(FAIL);
                    message.sendToTarget();
                }
            }
        );
    }

    /*Fonction pour envoyer une demande de connection au serveur.
     * Les listener sont appeles dans le threadUI*/
    public static void sendForUi(final Context context, final OnConnectionSuccess onConnectionSuccess) {
        sendForUi(context, onConnectionSuccess, null);
    }
}
