package com.gumeteapps.citation.authantication;

import android.content.Context;
import android.content.SharedPreferences;

/*Cette classe permet de modifier et obtenir le token courant*/
public class TokenManager {

    public static final String TAG = "mydebug";

    // Le fichier de preference contenant le token.
    private static String TOKEN_LOCATION = "com.gumeteapps.citation.token";
    // La cle du token dans le fichier de preference.
    private static String TOKEN_KEY = "token";

    /*Retourne le token courant. retourne null s'il n'y a pas de token*/
    public static String getToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TOKEN_LOCATION, Context.MODE_PRIVATE);

        return sharedPref.getString(TOKEN_KEY, null);
    }

    /*Change le token courant*/
    public static void saveToken(Context context, String token) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TOKEN_LOCATION, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TOKEN_KEY, token);
        editor.apply();
    }

}
