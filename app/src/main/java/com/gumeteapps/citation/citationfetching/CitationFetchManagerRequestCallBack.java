package com.gumeteapps.citation.citationfetching;

import android.content.Context;

import com.gumeteapps.citation.authantication.AuthRequestSendManager;
import com.gumeteapps.citation.networkfeatures.CronetRequestCallBack;

/*Le CallBack pour la requette du CitationFetchManager*/
public class CitationFetchManagerRequestCallBack extends CronetRequestCallBack {

    public static final String TAG = "mydebug";

    private Context context;

    CitationFetchManagerRequestCallBack(Context context) {
        this.context = context;
    }

    @Override
    public void onSuccess(String response) {
        // On notifie au CitationFetchManager que la reponse est bonne
        CitationFetchManager.instance.notifyCitation(
                CitationFetchManager.REQUEST_RESULT_SUCCESS,
                response);
    }

    @Override
    public void onFailure(int code) {
        if(code >= 400) {
            // Le serveur dit qu'on n'est pas connecte
            // On fait une demande de connection
            AuthRequestSendManager.send(this.context, new AuthRequestSendManager.OnConnectionSuccess() {
                @Override
                public void onConnect() {
                    // Si on est bien connecte, on reessaie.
                    if(! CitationFetchManager.instance.reTry(context)) {
                        // Si on a atteint le nombre max de reesaie,
                        // On notifie au CitationFetchManager que la reponse n'est pas bonne
                        CitationFetchManager.instance.notifyCitation(
                                CitationFetchManager.REQUEST_RESULT_FAIL,
                                null);
                    }
                }
            });
        }
    }
}
