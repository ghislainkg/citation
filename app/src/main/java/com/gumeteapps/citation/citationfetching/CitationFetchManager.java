package com.gumeteapps.citation.citationfetching;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import com.gumeteapps.citation.authantication.TokenManager;
import com.gumeteapps.citation.networkfeatures.CronetUtils;

/*Cette classe est gestionnaire de taches qui communique avec l' Thread UI
* Elle permet au Thread UI de lancer une requette dans une autre thread et d'obtenir et
* afficher le resultat de la requette */
public class CitationFetchManager {

    public static final String TAG = "mydebug";

    /*L'instance du singleton*/
    public static CitationFetchManager instance = new CitationFetchManager();

    // The url to receive citation
    //private static final String SERVER_URL = "https://warm-shore-29934.herokuapp.com/";

    // Le type de la requete actuelle
    private int requestType;
    private static final int REQUETE_TYPE_SEND_CITATION = 0;
    public static final String SERVER_URL_SEND_CITATION = "https://warm-shore-29934.herokuapp.com/addcitation";
    //public static final String SERVER_URL_SEND_CITATION = "http://192.168.43.251:5000/addcitation";
    private final int REQUETE_TYPE_REQUEST_CITATION = 1;
    public static final String SERVER_URL_REQUEST_CITATION = "https://warm-shore-29934.herokuapp.com/getcitation";
    //public static final String SERVER_URL_REQUEST_CITATION = "http://192.168.43.251:5000/getcitation";

    // La requete en cours de traitement. Elle est conservee pour pouvoir etre renvoyer si besoin.
    private String requete;
    // Le nombre de ressaie d'envoie de la requete
    private int retryCount;
    // Le nombre maximum de reessaie
    private static final int RETRY_MAX_COUNT = 3;

    /*Handler pour les recuperations de citation*/
    private static Handler handler;
    private OnEndListener onEndListener;
    static final int REQUEST_RESULT_SUCCESS = 0;
    static final int REQUEST_RESULT_FAIL = 1;

    private CitationFetchManager() {
        // On initialise le handler
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                if(onEndListener != null) {
                    switch (msg.what) {
                        case REQUEST_RESULT_SUCCESS:
                            String citation = (String) msg.obj;
                            onEndListener.onResult(citation);
                            break;
                        case REQUEST_RESULT_FAIL:
                            onEndListener.onFail();
                    }
                }
                super.handleMessage(msg);
            }
        };
    }

    /*Interface pour notify que la reponse a la requette est arrive*/
    public interface OnEndListener {
        void onResult(String res);
        void onFail();
    }

    /*Retourne une requete d'envoie de citation prete.
     * Retourne null si on n'a pas de token.*/
    public static String getSendCitationRequest(Context context, String citation) {
        String type = "a";
        String token = TokenManager.getToken(context);
        if(token == null) {
            return null;
        }
        return type+"|"+token+"|"+citation;
    }

    /*Cette fonction retourne une requete prete a etre envoyer.
    * Elle retrouve le token courant. et null s'il n'y en a pas.*/
    public static String getCitationRequest(Context context) {
        String type = "r";
        String token = TokenManager.getToken(context);
        if(token == null) {
            return null;
        }
        String content = "no";

        Log.i(TAG, "getCitationRequest: Token : "+token);

        return type+"|"+token+"|"+content;
    }

    // Fonction pour lancer la requette de reception de citation.
    // retourne -1 si on n'est pas connecte. et o si oui
    public int fetch(Context context, OnEndListener onFetchListener) {
        instance.onEndListener = onFetchListener;

        this.requete = getCitationRequest(context); // On recupere conserve la requete
        if(this.requete == null) {
            return -1;
        }
        this.retryCount = 0; // Pas encore de reessaie
        this.requestType = REQUETE_TYPE_REQUEST_CITATION;

        CronetUtils.makeRequest(context, SERVER_URL_REQUEST_CITATION, this.requete, new CitationFetchManagerRequestCallBack(context));
        return 0;
    }

    // Fonction pour lancer la requette d'envoie de citation.
    // retourne -1 si on n'est pas connecte. et o si oui
    public int send(Context context, String citation, OnEndListener onFetchListener) {
        instance.onEndListener = onFetchListener;

        this.requete = getSendCitationRequest(context, citation); // On recupere conserve la requete
        if(this.requete == null) {
            return -1;
        }
        this.retryCount = 0; // Pas encore de reessaie
        this.requestType = REQUETE_TYPE_SEND_CITATION;

        CronetUtils.makeRequest(context, SERVER_URL_SEND_CITATION, this.requete, new CitationFetchManagerRequestCallBack(context));
        return 0;
    }

    boolean reTry(Context context) {
        if(this.retryCount >= RETRY_MAX_COUNT) {
            return false;
        }
        String url;
        switch (this.requestType) {
            case REQUETE_TYPE_REQUEST_CITATION:
                url = SERVER_URL_REQUEST_CITATION;
                break;
            case REQUETE_TYPE_SEND_CITATION:
                url = SERVER_URL_SEND_CITATION;
                break;
            default:
                return false;
        }
        CronetUtils.makeRequest(context, url, this.requete, new CitationFetchManagerRequestCallBack(context));
        return true;
    }

    // Le CitationFetchManagerRequestCallBack attacher a la requette
    // une fois les resultat de sa requette recu appelle cette methode
    // Pour transmettre ses resultats
    void notifyCitation(int result, String citation) {
        // Notification au handler qui vas envoyer les donnees au Thread UI via OnFetchListener
        Message message = handler.obtainMessage(result, citation);
        message.sendToTarget();
    }

}
