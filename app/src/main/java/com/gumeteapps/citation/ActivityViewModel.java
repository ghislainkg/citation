package com.gumeteapps.citation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ActivityViewModel extends ViewModel {

    public static final String TAG = "mydebug";

    private MutableLiveData<String> citationData;

    public ActivityViewModel(){
    }

    MutableLiveData<String> getCitationData() {
        if(citationData == null) {
            citationData = new MutableLiveData<>();
        }
        return this.citationData;
    }
}
